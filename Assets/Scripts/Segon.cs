﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segon : MonoBehaviour {

    private float temps = 0;
    public ParticleSystem particle;
    public float speed;
    private MyGamemanager suma ;
    private void Start()
    {
        suma = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<MyGamemanager>();
    }
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        temps += Time.deltaTime;
        if(temps <= 1)
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        else
            transform.Translate(Vector2.down * speed * Time.deltaTime);
        if(temps >= 1.5)
            temps = 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Finish" || collision.tag=="Bullet")
           
        {
            suma.AddHighscore();
            Dead();
        }
    }

    void Dead()
    {
        particle.Play();
        Destroy(this.gameObject);
      
    }
}
