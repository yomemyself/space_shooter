﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MyGamemanager : MonoBehaviour {
	public Text highscoreText;
	public Text livesText;
	private int lives;
	private int highscore;
	private static MyGamemanager instance;
    public bool starto = true;
	public GameObject Pausa;
    private Spawn2 s2;
    private Spawn3 s3;
    void Awake(){
		if (instance == null) {
			instance = this;
		}

      
    }
	void Update(){
		if (Input.GetKeyDown (KeyCode.P))
			Pause();
        s2.MyUpdate();
        s3.MyUpdate();
		}	
	
	public static MyGamemanager getInstance(){
		return instance;
	}
	// Use this for initialization
	void Start () {
		lives = 3;
		highscoreText.text = highscore.ToString ("D5");
		livesText.text = "x3";
		livesText.text = "x " + lives.ToString ();
        s2 = GameObject.FindGameObjectWithTag("Spawn2").GetComponent<Spawn2>();
        s2.Initialize();
        s3 = GameObject.FindGameObjectWithTag("Spawn3").GetComponent<Spawn3>();
        s3.Initialize();
    }
	public void LoseLives(){
		lives--;
        if (lives == 0)
        {
            GameOver();
        }
		livesText.text = "x" + lives.ToString ();
	}
	// Update is called once per frame
	public void AddHighscore(){
		highscore += 10;
		highscoreText.text = highscore.ToString ("D5");
	}
	public void Pause(){
		{

			Pausa.SetActive (true);

		}
	}
    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
	public void Vuelta(){
		Time.timeScale = 1;
		Pausa.SetActive (false);
	}
	public void Salir(){
		SceneManager.LoadScene("Menu");
	}
    public void startgame()
    {
        starto = true;
    }
}
