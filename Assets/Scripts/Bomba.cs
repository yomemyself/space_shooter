﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour
{

    private float temps = 0;
    public float speed;
    private MyGamemanager suma;
    public GameObject enemyBullet;
    private Vector2 posbala;
    // Use this for initialization
    void Start()
    {
        suma = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<MyGamemanager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.down * speed * Time.deltaTime);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Finish" || collision.tag == "Bullet")

        {
            suma.AddHighscore();
       
            
            Dead();
        }
    }

    void Dead()
    {

        Destroy(this.gameObject);

    }
}
