﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBullet : MonoBehaviour
{
    public float speed;
    public bool shooting = false;
    public Vector3 iniPos;
    public AudioSource laser;
    private float temps=0;
    void Start()
    {
    

    }

    public void Shot(Vector3 position, float direction)
    {
        transform.position = position;
        shooting = true;
        transform.rotation = Quaternion.Euler(0, 0, direction);
        laser.Play();
    }

    // Update is called once per frame
    protected void Update()
    {

        temps += Time.deltaTime;
        if(shooting)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }
        if (temps >= 5)
            Reset();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Finish" || other.tag == "Meteor" || other.tag == "enemy")
        {


            Reset();
        }

    }

    public void Reset()
    {
        transform.position = iniPos;
        shooting = false;
    }
}
