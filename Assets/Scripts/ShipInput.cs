﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour {
	private Vector2 axis;

	public PlayerBehaviour ship;
	public Weapons weapons;
    private bool muerto;
	
	// Update is called once per frame
	void Update () {
		axis.x = Input.GetAxis ("Horizontal");
		axis.y = Input.GetAxis ("Vertical");

		ship.SetAxis (axis);

		if (Input.GetButton ("Fire1")) {
            if(muerto==true)
                return;
			weapons.ShotWeapon();
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			weapons.NextCartridge ();
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			weapons.PreviousWeapon ();
		}
		if(Input.GetKeyDown(KeyCode.P)){
			Time.timeScale = 0;

	}
   
    }
    public void Dead()
    {

        muerto = true;
    }
    public void Alive()
    {
        muerto = false;
    }
}
