﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CacheDisparo : MonoBehaviour
{
    EnemyBullet[] disparo;
    int currentBalas = 0;

    public CacheDisparo(GameObject prefabBullet, Vector3 position, Transform parent, int numBala)
    {
        disparo = new EnemyBullet[numBala];
        Vector3 bposition = position;

        for(int i = 0; i < numBala; i++)
        {
            disparo[i] = GameObject.Instantiate(prefabBullet, bposition, Quaternion.identity, parent).GetComponent<EnemyBullet>();
            disparo[i].name = prefabBullet.name + "_enemyBullet_" + i;
            position.x += 1;
        }




    }
    public EnemyBullet GetBullet()
    {
        if(currentBalas > disparo.Length - 1)
        {
            currentBalas = 0;
        }

        return disparo[currentBalas++];
    }
}
