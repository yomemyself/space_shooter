﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    private bool deployed;
    public Vector2 movespeed;
    public Transform graphics;
    private Collider2D coll;
    private Vector3 inipos;
    public ParticleSystem particle;
    public AudioSource audioExplosion;
	private BoxCollider2D thecoll;
    private Vector2 posbala;
    public GameObject enemyBullet;
    private MyGamemanager suma;
    private bool dead;
    private float time = 0;
    // Use this for initialization
	void Awake(){
		thecoll = GetComponent<BoxCollider2D> ();
	}
    void Start()
    {
        suma = GameObject.FindGameObjectWithTag("Gamemanager").GetComponent<MyGamemanager>();
        inipos = transform.position;
    
    }

    // Update is called once per frame
    void Update()
    {
        if (dead == true)
            return;
        time += Time.deltaTime;
		if (deployed) {
			transform.Translate(movespeed.x * Time.deltaTime, movespeed.y * Time.deltaTime, 0);
            enemyBullet.transform.Translate(movespeed.x * Time.deltaTime, movespeed.y * Time.deltaTime, 0);
            posbala = transform.position;
            if(time>4.0f)
            {
                time -= 4.0f;
                EnemyBullet temp=GameObject.Instantiate(enemyBullet, posbala, Quaternion.identity, null).GetComponent<EnemyBullet>();
                
                temp.Shot(posbala, 0);
            }
        }
      
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.tag == "Bullet"|| collision.tag =="Player") {
			graphics.gameObject.SetActive (false);
            dead = true;
			thecoll.enabled=false;
			particle.Play ();
            suma.AddHighscore();
			audioExplosion.Play ();
			Invoke ("Reset",2);
		}
    }

	private void Reset(){
		graphics.gameObject.SetActive (true);
		thecoll.enabled=true;
		transform.position = inipos;
        dead = false;
	}
	public void LaunchEnemy(Vector3 position,Vector2 direction){
		transform.position = position;
		deployed = true;

		movespeed = direction;

		graphics.gameObject.SetActive (true);
		thecoll.enabled = true;
	}
}
    
