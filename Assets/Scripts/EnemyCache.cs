﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCache {
	private Enemies[] enemies;
	private int currentenemy = 0;


	public EnemyCache(GameObject prefabEnemy, Vector3 position, Transform parent, int numenemies){
		enemies = new Enemies[numenemies];

		Vector3 tmpPosition = position;
		for(int i=0;i<numenemies;i++){
			enemies[i] = GameObject.Instantiate (prefabEnemy, tmpPosition, Quaternion.identity, parent).GetComponent<Enemies>();
			enemies[i].name = prefabEnemy.name+"_enemy_" + i;
			tmpPosition.x += 1;
		}
	}

	public Enemies GetEnemy(){
		if (currentenemy > enemies.Length - 1) {
			currentenemy = 0;
		}

		return enemies [currentenemy++];
	}
}
